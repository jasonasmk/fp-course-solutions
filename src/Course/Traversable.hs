{-# LANGUAGE InstanceSigs        #-}
{-# LANGUAGE NoImplicitPrelude   #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Course.Traversable where

import           Course.Applicative
import           Course.Compose
import           Course.Core
import           Course.ExactlyOne
import           Course.Functor
import           Course.List
import           Course.Optional

-- | All instances of the `Traversable` type-class must satisfy two laws. These
-- laws are not checked by the compiler. These laws are given as:
--
-- * The law of naturality
--   `∀f g. f . traverse g ≅ traverse (f . g)`
--
-- * The law of identity
--   `∀x. traverse ExactlyOne x ≅ ExactlyOne x`
--
-- * The law of composition
--   `∀f g. traverse ((g <$>) . f) ≅ (traverse g <$>) . traverse f`
class Functor t => Traversable t where
  traverse ::
    Applicative f =>
    (a -> f b)
    -> t a
    -> f (t b)

instance Traversable List where
  traverse ::
    Applicative f =>
    (a -> f b)
    -> List a
    -> f (List b)
  traverse f =
    -- so for the folding function, it produces the f b value, and via a functor
    -- takes the (:.) <$> f a , to be f ( a:. ) of type f (List b) which is then applied via <*> to
    -- the accumulator
    foldRight (\a b -> (:.) <$> f a <*> b) (pure Nil)

instance Traversable ExactlyOne where
  traverse ::
    Applicative f =>
    (a -> f b)
    -> ExactlyOne a
    -> f (ExactlyOne b)
  traverse afb (ExactlyOne a) = pure pure <*> afb a

instance Traversable Optional where
  traverse ::
    Applicative f =>
    (a -> f b)
    -> Optional a
    -> f (Optional b)
  traverse _ Empty      = pure Empty
  traverse afb (Full a) = pure pure <*> afb a
-- | Sequences a traversable value of structures to a structure of a traversable value.
--
-- >>> sequenceA (ExactlyOne 7 :. ExactlyOne 8 :. ExactlyOne 9 :. Nil)
-- ExactlyOne [7,8,9]
--
-- >>> sequenceA (Full (ExactlyOne 7))
-- ExactlyOne (Full 7)
--
-- >>> sequenceA (Full (*10)) 6
-- Full 60
sequenceA ::
  (Applicative f, Traversable t) =>
  t (f a)
  -> f (t a)
-- looks suspiciously similar to the traverse function. let's try to find something that would typecheck.
-- t (f a) -> f ( t a ) has to be equal to (a -> fb ) -> t a -> f ( t a )
-- In order for this to happen, a = f a and b = a which would yield us (f a -> f a) -> t (f a) -> f (t a)
-- Obviously the first one is id. so traverse id . If you think about it, it also makes sense implementation wise.
-- for a list like the one in the first example, you traverse and apply a function f a -> f a which kneads the monadic effect and takes it
-- out of the list due to how traverse works.
sequenceA = traverse id

instance (Traversable f, Traversable g) =>
  Traversable (Compose f g) where
-- Implement the traverse function for a Traversable instance for Compose
  traverse afb (Compose wrapped) = pure Compose <*> traverse (traverse afb) wrapped

-- | The `Product` data type contains one value from each of the two type constructors.
data Product f g a =
  Product (f a) (g a)

instance (Functor f, Functor g) =>
  Functor (Product f g) where
-- Implement the (<$>) function for a Functor instance for Product
  (<$>) ab (Product w1 w2) = Product (ab <$> w1) (ab <$> w2)

instance (Traversable f, Traversable g) =>
  Traversable (Product f g) where
-- Implement the traverse function for a Traversable instance for Product
  traverse afb (Product w1 w2) = pure Product <*> (afb `traverse` w1) <*> (afb `traverse` w2)

-- | The `Coproduct` data type contains one value from either of the two type constructors.
data Coproduct f g a =
  InL (f a)
  | InR (g a)

instance (Functor f, Functor g) =>
  Functor (Coproduct f g) where
-- Implement the (<$>) function for a Functor instance for Coproduct
  (<$>) ab (InL w) = InL (ab <$> w)
  (<$>) ab (InR w) = InR (ab <$> w)

instance (Traversable f, Traversable g) =>
  Traversable (Coproduct f g) where
-- Implement the traverse function for a Traversable instance for Coproduct
  -- traverse afb p = sequenceA $ afb <$> p
  traverse afb (InL w) = pure InL <*> (afb `traverse` w)
  traverse afb (InR w) = pure InR <*> (afb `traverse` w)
