# Solutions to the fp-course
Original readme and source (without answers) can be found below.



# Functional Programming Course

![Haskell logo](https://haskell-lang.org/static/img/logo.png?etag=rJR84DMh)

### Written by Tony Morris & Mark Hibberd for Data61

### With contributions from individuals (thanks!)

This version is maintained by Chris Allen at [https://github.com/bitemyapp/fp-course](https://github.com/bitemyapp/fp-course)